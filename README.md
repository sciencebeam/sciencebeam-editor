# ScienceBeam Editor

A prototype that connects ScienceBeam to Libero Editor

# Quick Start

You can launch the entire demo using....

```
docker-compose up
```

It takes a while to start, around 20 seconds, but then once ready you can open http://localhost:4000 in your browser.